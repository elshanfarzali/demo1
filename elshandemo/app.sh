#!/usr/bin/env bash

#First: Update apt base and install java
sudo apt-get update --fix-missing -y && sudo apt-get install openjdk-14-jdk -y

#Clone repo to machine
git clone https://gitlab.com/elshanfarzali/demo1.git && cd "$(basename "$_" .git)"

#Set mvnw executable
chmod +x mvnw

#Run clean package
./mvnw clean package

#Copy target file user home folder
cp /home/appuser/demo1/target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar /home/appuser/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar
